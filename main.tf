# Infrastructure for the cloud resume challenge using terraform on aws


provider "aws" {
  region = "eu-west-2"  # Update with your desired region
}

#create s3 bucket
resource "aws_s3_bucket" "website_bucket" {
  bucket = "ebeh_website_bucket"
  acl = "public-read"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}



#create cloudfront
resource "aws_cloudfront_distribution" "my_distribution" {
  origin {
    domain_name = aws_s3_bucket.ebeh_website_bucket.website_endpoint
    origin_id   = "S3-Website-origin"
  }

  default_cache_behavior {
    target_origin_id = "S3-Website-origin"
    viewer_protocol_policy = "redirect-to-https"
  }

  enabled = true
  is_ipv6_enabled = true

  aliases = ["your-custom-domain.com"]
}


#create route 53 